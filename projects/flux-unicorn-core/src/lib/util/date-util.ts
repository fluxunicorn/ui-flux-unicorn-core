import * as moment_ from 'moment';

export class DateUtil {

  static getDatesBetween(startDate: Date, endDate: Date) {
    const dateArray = [];
    const currentDate = new Date(startDate);
    endDate = new Date(endDate);
    while (currentDate <= endDate) {
      dateArray.push(new Date(currentDate));
      currentDate.setDate(currentDate.getDate() + 1);
    }
    return dateArray;
  }

  static getDaysBetween(startDate: Date, endDate: Date) {
    const oneDay = 24 * 60 * 60 * 1000; // Calculates milliseconds in a day
    return Math.abs((endDate.getTime() - startDate.getTime()) / (oneDay));
  }

  static getDateFromDay(day) {
    const date = new Date(); // initialize a date in `year-01-01`
    date.setDate(1);
    date.setMonth(0);
    return new Date(date.setDate(day)); // add the number of days
  }

  static isDayToday(month: number, day: number) {
    const moment = moment_;
    return moment().date() === day && (moment().month() + 1) === month;
  }

}
