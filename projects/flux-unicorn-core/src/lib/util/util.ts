export class Util {
  static checkIfNull(value) {
    return value === undefined || value === null;
  }

}
