export class DataObjectStructureItem {
  dataObjectStructureId: number;
  title: string;
  objectKey: string;
  entityStatus: string;
  childs: DataObjectStructureItem[];
}
