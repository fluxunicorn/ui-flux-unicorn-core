export interface Language {
  translateKey: string;
  value: string;
  flag: string;
  abbreviation: string;
}
