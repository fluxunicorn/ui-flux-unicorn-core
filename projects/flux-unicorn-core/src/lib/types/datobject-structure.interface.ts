import { DataObjectStructureItem } from './datobject-structure-item.interface';

export class DataObjectStructure {
  dataObjectStructureId: number;
  rootElement: DataObjectStructureItem;

}
