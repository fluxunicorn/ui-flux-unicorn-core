import { DataObjectFieldType } from './dataobject-field-type.enum';
import { DataObjectFieldValue } from './datobject-field-values.interface';
import { ValidatorType } from './validator-type.enum';
import { DataObject } from './datobject.interface';

export class DataObjectField {
  dataObjectFiledId: string;
  label: string;
  translationKey: string;
  value: string;
  relatedDataObject?: DataObject;
  type: DataObjectFieldType;
  dataObjectFieldValues: DataObjectFieldValue[] = [];
  dataObjectValidators: DataObjectValidator[] = [];
  dataObjectFilters?: DataObjectFilters[] = [];
}

export class DataObjectValidator {
  foreignId: number;
  stringCriteria: string;
  numberCriteria: number;
  patternTranslation: string;
  validatorType: ValidatorType;
  dataObjectValidatorID?: string;
}

export class DataObjectFilters {
  enabled: boolean;
  field: string;
  filter: string;
  value: string;
  allowNullValues: boolean;
}

