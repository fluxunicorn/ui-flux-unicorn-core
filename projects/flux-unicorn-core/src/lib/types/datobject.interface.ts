import { DataObjectField } from './datobject-field.interface';

export class DataObject {
  dataObjectID: string;
  title: string;
  baseValue: string;
  objectName: string;
  apiBaseUrl: string;
  microservice: string;
  entityStatus: any;
  dataObjectFields: DataObjectField[];
}
